package ast;
public class AstBool extends AstNode
{
    private Boolean val;

    public AstBool(Boolean val)
    {
        this.val = val;
    }

    public Boolean getVal()
    {
        return val;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }

}

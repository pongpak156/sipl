package ast;
public class AstConditional extends AstNode
{
    private AstNode leftChild;
    private AstNode mainChild;
    private AstNode rightChild;

    public AstConditional(AstNode leftChild,AstNode mainChild, AstNode rightChild)
    {
        this.leftChild = leftChild;
        this.mainChild = mainChild;
        this.rightChild = rightChild;
    }

    public AstNode getLeftChild()
    {
        return leftChild;
    }
    
    public AstNode getMainChild()
    {
        return mainChild;
    }
    
    public AstNode getRightChild()
    {
        return rightChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

package ast;
public class AstConditionalOption extends AstNode
{
    private AstNode mainChild;

    public AstConditionalOption(AstNode mainChild)
    {
        this.mainChild = mainChild;
    }

    public AstNode getMainChild()
    {
        return mainChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

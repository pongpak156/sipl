package ast;
public class AstEInt extends AstNode
{
    private Integer val;

    public AstEInt(Integer val)
    {
        this.val = val;
    }

    public Integer getVal()
    {
        return val;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }

}

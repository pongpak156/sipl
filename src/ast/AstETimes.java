package ast;
public class AstETimes extends AstNode
{
    private AstNode leftChild;
    private AstNode rightChild;

    public AstETimes(AstNode leftChild, AstNode rightChild)
    {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public AstNode getLeftChild()
    {
        return leftChild;
    }

    public AstNode getRightChild()
    {
        return rightChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

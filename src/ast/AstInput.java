package ast;
public class AstInput extends AstNode
{
    private AstNode mainChild;

    public AstInput(AstNode mainChild)
    {
        this.mainChild = mainChild;
    }

    public AstNode getMainChild()
    {
        return mainChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

package ast;
public class AstLooping extends AstNode
{
    private AstNode leftChild;
    private AstNode rightChild;

    public AstLooping(AstNode leftChild, AstNode rightChild)
    {
        this.leftChild = leftChild;
        this.rightChild = rightChild;
    }

    public AstNode getLeftChild()
    {
        return leftChild;
    }

    public AstNode getRightChild()
    {
        return rightChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

package ast;
public abstract class AstNode
{
    public abstract void accept(AstVisitor v) ;
}

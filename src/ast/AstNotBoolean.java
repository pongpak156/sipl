package ast;
public class AstNotBoolean extends AstNode
{
    private AstNode mainChild;

    public AstNotBoolean(AstNode mainChild)
    {
        this.mainChild = mainChild;
    }

    public AstNode getMainChild()
    {
        return mainChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

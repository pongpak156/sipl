package ast;
public class AstOutput extends AstNode
{
    private AstNode mainChild;

    public AstOutput(AstNode mainChild)
    {
        this.mainChild = mainChild;
    }

    public AstNode getMainChild()
    {
        return mainChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

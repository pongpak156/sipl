package ast;
public class AstString extends AstNode
{
    private String val;

    public AstString(String val)
    {
        this.val = val;
    }

    public String getVal()
    {
        return val;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }

}

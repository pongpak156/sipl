package ast;
public class AstUnary extends AstNode
{
    private AstNode mainChild;

    public AstUnary(AstNode mainChild)
    {
        this.mainChild = mainChild;
    }

    public AstNode getMainChild()
    {
        return mainChild;
    }

    public void accept(AstVisitor v)
    {
        v.visit(this);
    }
}

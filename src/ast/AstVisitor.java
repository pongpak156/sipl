package ast;
public interface AstVisitor
{
    abstract void visit(AstEPlus e);
    abstract void visit(AstEMinus e);
    abstract void visit(AstETimes e);
    abstract void visit(AstEDivide e);
    abstract void visit(AstEInt e);
    abstract void visit(AstBlock e);
    abstract void visit(AstString e);
    abstract void visit(AstRelop e);
    abstract void visit(AstDeclaration e);
    abstract void visit(AstAssignment e);
    abstract void visit(AstSequence e);
    abstract void visit(AstConditional e);
    abstract void visit(AstConditionalOption e);
    abstract void visit(AstLooping e);
    abstract void visit(AstInput e);
    abstract void visit(AstOutput e);
    abstract void visit(AstUnary e);
    abstract void visit(AstNotBoolean e);
    abstract void visit(AstBool e);
}

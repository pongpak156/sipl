package ast;
public class PrettyPrint implements AstVisitor
{
    public void visit(AstEPlus e)
    {
        System.out.printf("(");
        e.getLeftChild().accept(this);
        System.out.printf(" + ");
        e.getRightChild().accept(this);
        System.out.printf(")");
    }

    public void visit(AstEMinus e)
    {
        System.out.printf("(");
        e.getLeftChild().accept(this);
        System.out.printf(" - ");
        e.getRightChild().accept(this);
        System.out.printf(")");
    }

    public void visit(AstETimes e)
    {
        System.out.printf("(");
        e.getLeftChild().accept(this);
        System.out.printf(" * ");
        e.getRightChild().accept(this);
        System.out.printf(")");
    }
    
    public void visit(AstEDivide e)
    {
        System.out.printf("(");
        e.getLeftChild().accept(this);
        System.out.printf(" / ");
        e.getRightChild().accept(this);
        System.out.printf(")");
    }

    public void visit(AstEInt e)
    {
        System.out.printf("%d", e.getVal());
    }

    public void visit(AstBlock e)
    {
        System.out.println("begin");
        e.getLeftChild().accept(this);
        e.getRightChild().accept(this);
        System.out.println("");
        System.out.printf("end");
    }

    public void visit(AstRelop e)
    {
        System.out.printf("(");
        e.getLeftChild().accept(this);
        System.out.printf(" ");
        e.getMainChild().accept(this);
        System.out.printf(" ");
        e.getRightChild().accept(this);
        System.out.printf(")");
    }

    public void visit(AstDeclaration e)
    {
        e.getLeftChild().accept(this);
        System.out.printf(" ");
        e.getMainChild().accept(this);
        System.out.println(";");
        e.getRightChild().accept(this);
    }


    public void visit(AstAssignment e)
    {
        e.getLeftChild().accept(this);
        System.out.printf(" := ");
        e.getRightChild().accept(this);
    }

    public void visit(AstSequence e)
    {
        e.getLeftChild().accept(this);
        System.out.println(";");
        e.getRightChild().accept(this);
    }

    public void visit(AstConditional e)
    {
    	System.out.printf("if ");
        e.getLeftChild().accept(this);
        System.out.println(" then");
        e.getMainChild().accept(this);
        e.getRightChild().accept(this);
    }

    public void visit(AstConditionalOption e)
    {
        System.out.println(" else ");
        e.getMainChild().accept(this);
    }

    public void visit(AstLooping e)
    {
        System.out.println("while ");
        e.getLeftChild().accept(this);
        System.out.println(" do ");
        e.getRightChild().accept(this);
    }

    public void visit(AstInput e)
    {
        System.out.printf("input ");
        e.getMainChild().accept(this);
    }

    public void visit(AstOutput e)
    {
        System.out.printf("output ");
        e.getMainChild().accept(this);
    }

    public void visit(AstString e)
    {
        System.out.printf("%s", e.getVal());
    }
    
    public void visit(AstBool e)
    {
        System.out.printf("%b", e.getVal());
    }

    public void visit(AstUnary e)
    {
        System.out.printf("-");
        e.getMainChild().accept(this);
    }
    public void visit(AstNotBoolean e)
    {
        System.out.printf("not ");
        e.getMainChild().accept(this);
    }
}

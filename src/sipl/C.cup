package sipl;
import java_cup.runtime.*;
import java.util.ArrayList;
import ast.*;

parser code {:

    public void report_error(String message, Object info)
    {
        StringBuffer m = new StringBuffer("Error");

        if (info instanceof java_cup.runtime.Symbol)
        {
            java_cup.runtime.Symbol s = ((java_cup.runtime.Symbol) info);

            if (s.left >= 0)
            {
                m.append(" in line ")
                        .append(s.left+1);
                if (s.right >= 0)
                {
                    m.append(", column ")
                            .append(s.right+1);
                }
            }
            if (s.value != null)
            {
                m.append(", value \"")
                        .append(s.value)
                        .append("\"");
            }
        }

        m.append(" : ")
                .append(message);

        System.err.println(m);
    }

//    public void report_fatal_error(String message, Object info)
//    throws Exception
//    {
//        done_parsing();
//        report_error(message, info);
//        throw new Exception("Can't recover from previous error(s)");
//    }

:};
   
terminal	String	AND;
terminal	String	DO;
terminal	String	ELSE;
terminal	Boolean	BOOL;
terminal	String	IF;
terminal	String	NOT;
terminal	String	OR;
terminal	String	OUTPUT;
terminal	String	INPUT;
terminal	String	THEN;
terminal	String	VAR;
terminal	String	WHILE;

terminal	String	INTVAR;

terminal	String	RELOP;

terminal	String	ASSIGN;
terminal	String	SEMI;

terminal	String 	BEGIN;
terminal	String	END;
terminal	Integer	INT;

terminal	PLUS;
terminal	MINUS;
terminal	TIMES;
terminal	OBELUS;
terminal	String	LEFTPARENTHESIS;
terminal	String	RIGHTPARENTHESIS;

non terminal	AstNode	Boolean_Expressions;
non terminal	AstNode	T_Boolean_Expressions;
non terminal	AstNode	F_Boolean_Expressions;

non terminal	AstNode	Commands;
non terminal	AstNode	SubCommands;

non terminal	AstNode	Declaration;
non terminal	AstNode	Assignment;
non terminal	AstNode	Sequence;
non terminal	AstNode	Conditional;
non terminal	AstNode	ConditionalOption;
non terminal	AstNode	Looping;
non terminal	AstNode	Output;
non terminal	AstNode	Input;

non terminal	AstNode	Block;
non terminal	AstNode	Integer_Expressions;
non terminal	AstNode	T_Expressions;
non terminal	AstNode	X_Expressions;
non terminal	AstNode	F_Expressions;

precedence left SEMI; 
precedence left ELSE; 

Block      ::=   BEGIN:b Declaration:d Commands:c END:e
                    {:
                        RESULT = new AstBlock(d,c);
                    :}
                 	;
                
Integer_Expressions		::=   Integer_Expressions:E PLUS:o T_Expressions:T 
                    {:
                        RESULT = new AstEPlus(E,T);
                    :}
                 	|  Integer_Expressions:E MINUS:o T_Expressions:T 
                    {:
                        RESULT = new AstEMinus(E,T);
                    :}
                	|  T_Expressions:T 
                    {:
                        RESULT = T;
                    :}
                   	;
                   	
T_Expressions		::=		T_Expressions:T TIMES:o X_Expressions:F
                    {:
                        RESULT = new AstETimes(T,F);
                    :}
                 	|  T_Expressions:T OBELUS:o X_Expressions:F  
                    {:
                        RESULT = new AstEDivide(T,F);
                    :}
                	|  X_Expressions:F 
                    {:
                        RESULT = F;
                    :}
                    ;
X_Expressions		::=	MINUS:m F_Expressions:F
                    {:
                        RESULT = new AstUnary(F);
                    :}
                	|  F_Expressions:F 
                    {:
                        RESULT = F;
                    :}
                    ;
                    
F_Expressions		::=   LEFTPARENTHESIS:l Integer_Expressions:E RIGHTPARENTHESIS:r
                    {:
                        RESULT = E;
                    :}
                    |  INT:n
                    {:
                        RESULT = new AstEInt(n);
                    :}
                    |  INTVAR:vari 
                    {:
                        RESULT = new AstString(vari);
                    :}
                 	;
              
Boolean_Expressions		::=  Boolean_Expressions:b1 OR:o  T_Boolean_Expressions:b2
                    {:
                        RESULT = new AstRelop(b1,new AstString(o),b2);
                    :}
                    |  Boolean_Expressions:b1 AND:o  T_Boolean_Expressions:b2
                    {:
                        RESULT = new AstRelop(b1,new AstString(o),b2);
                    :} 
                    |  T_Boolean_Expressions:b
                    {:
                        RESULT = b;
                    :} 
                 	;
                 
T_Boolean_Expressions	::= 	NOT  F_Boolean_Expressions:b
                    {:
                        RESULT = new AstNotBoolean(b);
                    :}
                    |
                    F_Boolean_Expressions:b
                    {:
                        RESULT = b;
                    :}
                    ;
                    
F_Boolean_Expressions ::=	Integer_Expressions:e1 RELOP:r Integer_Expressions:e2
                    {:
                        RESULT = new AstRelop(e1,new AstString(r),e2);
                    :}
                    |  LEFTPARENTHESIS:l Boolean_Expressions:b RIGHTPARENTHESIS:r
                    {:
                        RESULT = new AstRelop(new AstString(l),b,new AstString(r));
                    :}
                    | BOOL:b
                    {:
                        RESULT = new AstBool(b);
                        System.out.println("found a bool");
                    :}
                    ;

Commands			::= Sequence:s
                    {:
                        RESULT = s;
                    :}
                    |  SubCommands:x
                    {:
                        RESULT = x;
                    :}
                 ; 
SubCommands			::= Assignment:a
                    {:
                        RESULT = a;
                    :}
                    |  Conditional:c
                    {:
                        RESULT = c;
                    :}
                    |  Looping:l
                    {:
                        RESULT = l;
                    :}
                    |  Output:o
                    {:
                        RESULT = o;
                    :}
                    |  Input:in
                    {:
                        RESULT = in;
                    :}
                 ;      
                         
Declaration			::=   VAR:v INTVAR:iv SEMI:s Declaration:d
                    {:
                        RESULT = new AstDeclaration(new AstString(v),new AstString(iv),d);
                    :}
                    |
                    {:
                        RESULT = new AstString("");
                    :}
                 ;    
Assignment			::=   INTVAR:v ASSIGN:o Integer_Expressions:E
                    {:
                        RESULT = new AstAssignment(new AstString(v),E);
                    :}
                 ;
Sequence			::=   Commands:c1 SEMI:s Commands:c2
                    {:
                        RESULT = new AstSequence(c1,c2);
                    :}
                 ;
                 
Conditional			::=   IF:i1 Boolean_Expressions:b THEN:t SubCommands:c ConditionalOption:co
                    {:
                        RESULT = new AstConditional(b,c,co);
                    :}
                    | IF:i1 Boolean_Expressions:b THEN:t Block:bl ConditionalOption:co
                    {:
                        RESULT = new AstConditional(b,bl,co);
                    :}
                    ;
ConditionalOption	::= ELSE:e Block:b
                    {:
                        RESULT = new AstConditionalOption(b);
                    :}
                    | 	ELSE:e SubCommands:c
                    {:
                        RESULT = new AstConditionalOption(c);
                    :}
                    |
                    {:
                        RESULT = new AstString("");
                    :}
                 ;
Looping			::=   WHILE:w Boolean_Expressions:b DO:d SubCommands:sc
                    {:
                        RESULT = new AstLooping(b,sc);
                    :}
                    |  WHILE:w Boolean_Expressions:b DO:d Block:bl
                    {:
                        RESULT = new AstLooping(b,bl);
                    :}
                 ;
Output			::=   OUTPUT:o Integer_Expressions:E
                    {:
                        RESULT = new AstOutput(E);
                    :}
                 ;
Input			::=   INPUT:o INTVAR:v
                    {:
                        RESULT = new AstInput(new AstString(v));
                    :}
                 ;
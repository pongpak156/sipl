package sipl;
import java.io.File;
import java.io.FileReader;

import ast.*;

public class Main
{
    static public void main(String argv[])
    {
    	File f = null;
    	if (argv.length != 1 || !(f = new File(argv[0])).canRead() || !f.isFile())
    	{
    		System.err.println("Usage: java -jar siplParse.jar <input_file_name>");
    		System.exit(1);
    	}

    	try
        {
            CCup p = new CCup(new Lexer(new FileReader(f)));
            AstNode a = null;
            try
            {
                a = (AstNode) p.parse().value;
            }
            catch (Exception e)
            {
                System.err.println("Error: " + e.getMessage());
                System.exit(1);
            }
            if (a == null)
            {
                System.err.println("Error: no result returned from parser");
                System.exit(1);
            }
            
            PrettyPrint pp = new PrettyPrint() ;
            a.accept(pp);
            
           

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

}



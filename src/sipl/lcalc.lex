package sipl;
import java_cup.runtime.*;
      
%%

%class Lexer

%line
%column
%unicode
%cup
%yylexthrow Exception

%{
    private Symbol symbol(int type)
    {
        return new Symbol(type, yyline, yycolumn);
    }

    private Symbol symbol(int type, Object value)
    {
        return new Symbol(type, yyline, yycolumn, value);
    }

%}

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]

comments = \/\/.*{LineTerminator}
intvar = [A-Za-z_][A-Za-z_0-9]*
int = 0 | [1-9][0-9]*
bool = true|false
relop = <|>|<=|>=|==|\!=
%%

"begin"		{ return symbol(sym.BEGIN,yytext()); }
"end"		{ return symbol(sym.END,yytext()); }
"and"		{ return symbol(sym.AND,yytext()); }
"do"		{ return symbol(sym.DO,yytext()); }
"else"		{ return symbol(sym.ELSE,yytext()); }
"if"		{ return symbol(sym.IF,yytext()); }
"input"		{ return symbol(sym.INPUT,yytext()); }
"not"		{ return symbol(sym.NOT,yytext()); }
"or"		{ return symbol(sym.OR,yytext()); }
"output"	{ return symbol(sym.OUTPUT,yytext()); }
"then"		{ return symbol(sym.THEN,yytext()); }
"var"		{ return symbol(sym.VAR,yytext()); }
"while"		{ return symbol(sym.WHILE,yytext()); }
{bool}		{ return symbol(sym.BOOL,new Boolean(yytext())); }
"+"			{ return symbol(sym.PLUS,yytext()); }
"-"			{ return symbol(sym.MINUS,yytext()); }
"*"			{ return symbol(sym.TIMES,yytext()); }
"/"			{ return symbol(sym.OBELUS,yytext()); }
"("			{ return symbol(sym.LEFTPARENTHESIS,yytext()); }
")"			{ return symbol(sym.RIGHTPARENTHESIS,yytext()); }
":="		{ return symbol(sym.ASSIGN,yytext()); }

{comments}	{}

{intvar}	{ return symbol(sym.INTVAR, yytext());}

";"		{ return symbol(sym.SEMI,yytext()); }

{int}		{ return symbol(sym.INT,new Integer(yytext())); }

{relop}		{ return symbol(sym.RELOP, yytext()); }

{WhiteSpace}           {}

[^]                    {
                         // CUP generates the "sym" class with constants for all
                         // the non-terminal symbols. It also generates an "error"
                         // non-terminal symbol, that should be used to return
                         // errors from the lexer. Any input character not
                         // recognised by anything other regular expression in
                         // the lexer is an error.
                         
                         return symbol(sym.error, yytext());
                       }
